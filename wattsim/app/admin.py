from django.contrib import admin
from wattsim.app.models import Station, Measurement


@admin.register(Station)
class StationAdmin(admin.ModelAdmin):
    list_display = ('id', 'user')
    fields = ('id', 'user')
    readonly_fields = ('id', )


@admin.register(Measurement)
class MeasurementAdmin(admin.ModelAdmin):
    list_display = ('id', 'station', 'recorded')
    fields = ('id', 'station', 'recorded', 'solar', 'wind', 'hydro')
    readonly_fields = ('id', 'recorded')
